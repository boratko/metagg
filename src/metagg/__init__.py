from __future__ import annotations
from loguru import logger
import attr
from typing import *
from collections import defaultdict

__all__ = [
    "Agg",
    "get_agg",
    "get_active_agg",
    "set_active_agg",
]


@attr.s(auto_attribs=True, collect_by_mro=True)
class Agg:
    name: str
    parent: Optional[Agg] = attr.ib(
        None, repr=lambda z: z.name if z is not None else "None"
    )
    _log_dict: Dict[str, list] = attr.ib(
        factory=lambda: defaultdict(list), init=False, repr=False
    )
    _children: Dict[str, Agg] = attr.ib(
        factory=dict, init=False, repr=lambda z: ", ".join(z)
    )

    def __attrs_post_init__(self):
        logger.debug(f"Created {self}")

    def collect(self, d: dict) -> dict:
        """
        Collect values to log upon commit.
        :param d: dictionary of metrics to collect
        :return: dictionary of currently collected metrics
        """
        for k, v in d.items():
            self._log_dict[k].append(v)
        return self._log_dict

    def commit(self) -> None:
        """If there are values to log, log them and clear the log."""
        self.clear()

    def clear(self) -> None:
        """Clear collected log values."""
        self._log_dict = defaultdict(list)

    def fully_qualified_names(self) -> Generator[str, None, None]:
        if self.parent is not None:
            yield from self.parent.fully_qualified_names
        yield self.name

    def has_collected_data(self) -> bool:
        return len(self._log_dict) > 0

    def get_agg(self, *names: str) -> Agg:
        """Return the Agg specified by `names`"""
        if len(names) == 0:
            return self
        if names[0] not in self._children:
            self._children[names[0]] = Agg(names[0], self)
        return self._children[names[0]].get_agg(*names[1:])

    def set_active(self) -> Agg:
        set_active_agg(self)
        return self


_ROOT_AGG = Agg("_root")
_ACTIVE_AGG = _ROOT_AGG

get_agg = _ROOT_AGG.get_agg


def get_active_agg() -> Agg:
    """Return the active Agg"""
    global _ACTIVE_AGG
    return _ACTIVE_AGG


def set_active_agg(agg: Agg) -> Agg:
    """Set the active AGG"""
    global _ACTIVE_AGG
    _ACTIVE_AGG = agg
    return _ACTIVE_AGG
