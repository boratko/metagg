import metagg


def test_agg_creation():
    agg = metagg.get_agg("First Agg")
    assert agg.parent == metagg.get_agg()
    assert metagg.get_agg()._children == {"First Agg": agg}


def test_direct_sub_agg_creation():
    first_sub_sub = metagg.get_agg("First Agg", "First Sub", "First Sub Sub")
    first_sub = metagg.get_agg("First Agg", "First Sub")
    assert first_sub_sub.parent == first_sub
    assert first_sub._children == {"First Sub Sub": first_sub_sub}


def test_object_sub_agg_creation():
    parent_agg = metagg.get_agg("First Agg")
    sub_agg = parent_agg.get_agg("Sub Agg")
    assert sub_agg == metagg.get_agg("First Agg", "Sub Agg")


def test_active_agg():
    root_agg = metagg.get_active_agg()
    assert root_agg == metagg.get_agg()
    sub_agg = root_agg.get_agg("Sub Agg")
    sub_agg.set_active()
    sub_logger_2 = metagg.get_active_agg()
    assert sub_agg == sub_logger_2
